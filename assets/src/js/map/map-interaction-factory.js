import stampit from 'stampit';
import MarkerFactory from '../map-api/leaflet/marker-api-factory';


const PrivilegedMethods = stampit({

  /**
   *
   * Creates interaction handler for leaflet map
   *
   * @memberOf module:Map/MapInteractionFactory
   * @instance
   *
   * @return void
   */
  init() {

    let mapHandler,
      markerHandler;

    /**
     *
     * Get map or return null if no map was created
     *
     * @memberOf module:Map/MapInteractionFactory
     * @function getMapHandler
     * @instance
     *
     * @return {object} MapApi/Leaflet/MapApiFactory instance
     */
    this.getMapHandler = function getMap() {
      return mapHandler;
    };

    /**
     *
     * Get map or return null if no map was created
     *
     * @memberOf module:Map/MapInteractionFactory
     * @function setMapHandler
     * @instance
     *
     * @param {object} MapApi/Leaflet/MapApiFactory instance
     */
    this.setMapHandler = function setMapHandler(handler) {
      mapHandler = handler;
    };

    /**
     *
     * Get map or return null if no map was created
     *
     * @memberOf module:Map/MapInteractionFactory
     * @function getMarker
     * @instance
     *
     * @return {?object} MapApi/Leaflet/MarkerApiFactory instance
     */
    this.getMarker = function getMarker() {
      return markerHandler || null;
    };

    /**
     *
     * Sets leaflet map instance value to be returned with getMap
     *
     * @memberOf module:Map/MapInteractionFactory
     * @function setMarker
     * @instance
     *
     * @param {object} MapApi/Leaflet/MarkerApiFactory instance
     * @return void
     */
    this.setMarker = function setMarker(marker) {
      markerHandler = marker;
    };
  }
});

const InitiateProps = stampit({

  /**
   *
   * Initializes handler with needed data.
   * Is called automatically with options passed to
   * main factory method / create method
   *
   * @ignore
   *
   * @param {object} mapHandler MapApi/Leaflet/MapApiFactory instance
   *
   * @return void
   */
  init: function init(mapHandler) {
    this.setMapHandler(mapHandler);
  },

  // properties: {
  //   data: {},
  //   options: {}
  // }
});


const PublicMethods = stampit({

  methods: {
    addPolygons(polygons, callback) {

      let mapObj = this.getMapHandler().getMap(),
        markerHandler,
        thisObj = this;


      // Add click handler to all invisible polygons
      polygons.forEach(polygon => {
        if (callback) {
          polygon.addTo(mapObj).on('click', function (ev) {
            let latlng = mapObj.mouseEventToLatLng(ev.originalEvent);

            thisObj.addClickMarker({
              lat: latlng.lat,
              lng: latlng.lng
            }, callback);
          });
        } else {
          polygon.addTo(mapObj);
        }
      });
    },

    addClickMarker(options, callback) {

      let mapObj = this.getMapHandler().getMap(),
        markerHandler = this.getMarker(),
        thisObj = this;

      if (!markerHandler) {
        markerHandler = MarkerFactory({
          data:{
            lat: options.lat,
            lng: options.lng,
            map: mapObj,
            // icon: {
            //   iconUrl: '/some-path.png'
            // }
          }
        });

        this.setMarker(markerHandler);
      } else {
        markerHandler.setPosition(options);
      }

      markerHandler.addInfoWindow('Lorem Ipsum<br>Hier ein Button zum klicken: <a class="js-click-marker" href="#">Button</a>');

      markerHandler.getItem().on('popupclose', function(){
        markerHandler.getItem().remove();
        thisObj.setMarker(null);
      });

      markerHandler.getItem().on('popupopen', function(event){

        var link = event.popup.getElement().getElementsByClassName('js-click-marker');

        link[0].addEventListener('click', function(event){
          event.preventDefault();

          callback.apply(this, [event, options.lat, options.lng]);
        });


      });

      markerHandler.openWindow();
    },

    addMarkers(markers, clickCallback) {
      let mapObj = this.getMapHandler().getMap();

      markers.forEach((marker, index) => {

        let markerHandler = MarkerFactory({
          data:{
            lat: marker.lat,
            lng: marker.lng,
            map: mapObj,
            icon: {
              iconUrl: marker.category.markericon.url,
              iconSize: [40,40],
              iconAnchor: [20,40],
              popupAnchor: [20,-5]
            }
          }
        });

        markerHandler.getItem().on('click', function(event){
          clickCallback.apply(this, [event, markerHandler, index]);
        });
      });
    }

  }

});


const MapInteractionFactory = stampit(PrivilegedMethods, InitiateProps, PublicMethods);


/**
 *
 * Leaflet maps interaction factory created using stampit
 *
 * @module Map/MapInteractionFactory
 *
 * Initializes interaction handler with needed data.
 *
 * @param {object} mapHandler MapApi/Leaflet/MapApiFactory instance
 *
 */
export default MapInteractionFactory;
