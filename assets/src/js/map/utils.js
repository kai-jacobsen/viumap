import difference from '@turf/difference';
import flip from '@turf/flip';
import {polygon as turfPolygon} from '@turf/helpers';

export default {

  /**
   *
   * Retrieves all polygons from FeatureCollection in geojson
   * data
   *
   * @param {object} geo json parsed geojson data
   *
   * @return {object} array of L.polygon instances
   */
  getPolygonsFromFeatureCollection(geo){

    let polygons = [];

    // Loop through all features of type polygon
    // and subtract them from main cover poly
    // using turf difference helper
    geo.features.forEach(feature => {
      let geometry = feature.geometry,
        flippedPolyCoord;

      if (geometry.type === 'Polygon') {

        flippedPolyCoord = flip(feature);

        // Add intersected polygon to map
        polygons.push(L.polygon(flippedPolyCoord.geometry.coordinates, {
          fillColor: '#fff',
          fillOpacity: 0.01,
          color: '#000',
          dashArray: '10, 5'
        }));
      }
    });

    return polygons;
  },

  /**
   *
   * Retrieve combined bounds of polygons
   *
   * @param {object} polygons array of L.polygon instances
   *
   * @return {object} L.latLngBounds
   */
  getPolygonBounds(polygons){
    let boundsObj = L.latLngBounds();

    // Loop through all features of type polygon
    // and subtract them from main cover poly
    // using turf difference helper
    polygons.forEach(polygon => {
      let coords = polygon.getBounds();
      boundsObj.extend(coords);
    });

    return boundsObj;
  },

  /**
   *
   * Gets main polygon which covers entire map and subtracts
   * shapes of polygons inside it
   *
   * @param {object} geo json parsed geojson data
   * @param {object} apiData complete api data object
   *
   * @return {object} L.polygon
   */
  getIntersectedPolygon(geo, apiData){
    let intersectedPoly,
      finalPoly,
      convertedCoordinates;

    // Create turfpolygon covering the map completely
    intersectedPoly = turfPolygon([[
      [-180, 90],
      [180, 90],
      [180, -90],
      [-180, -90],
      [-180, 90]
    ]]);

    // Loop through all features of type polygon
    // and subtract them from main cover poly
    // using turf difference helper
    geo.features.forEach(feature => {
      let geometry = feature.geometry;

      if (geometry.type === 'Polygon') {
        let polygon = turfPolygon(geometry.coordinates);

        intersectedPoly = difference(intersectedPoly, polygon);
      }
    });

    // GeoJSON coordinates are flipped compared to
    // leaflet, so they first need to be flipped back
    convertedCoordinates = flip(intersectedPoly);

    // Add intersected polygon to map, styled with apiData settings
    return L.polygon(convertedCoordinates.geometry.coordinates, {
      fillColor: apiData.poly_fill || '#fff',
      fillOpacity: apiData.poly_opacity || 0.7,
      stroke: false
    });
  }
};
