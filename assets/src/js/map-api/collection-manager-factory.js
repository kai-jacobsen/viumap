import stampit from 'stampit';
import collectionFactory from './collection-factory';


const InitiateProps = stampit({

  /**
   *
   *
   * @ignore
   *
   * @return void
   */
  init: function init() {
    this.collections = {};
  },

  properties: {
    collections: null
  }
});


const PublicMethods = stampit({
  methods: {

    /**
     *
     * Adds new collection
     *
     * @memberOf module:MapApi/CollectionManagerFactory
     * @instance
     * @todo add check if collection already exists
     *
     * @param {string} key collection key
     * @return {object} instance created with {@link module:MapApi/CollectionFactory|MapApi/CollectionFactory}
     */
    addCollection: function addCollection(key){
      let collection = collectionFactory();

      if (!this.collectionExists(key)) {
        this.collections[key] = collection;
      }
      return collection;
    },

    /**
     *
     * Retrieves collection
     *
     * @memberOf module:MapApi/CollectionManagerFactory
     * @instance
     *
     * @param {string} key collection key
     * @return {object} instance created with {@link module:MapApi/CollectionFactory|MapApi/CollectionFactory}
     */
    getCollection: function getCollection(key) {
      let collection = null;

      if (this.collectionExists(key)) {
        collection = this.collections[key];
      }

      return collection;
    },

    /**
     *
     * Deletes collection
     *
     * @memberOf module:MapApi/CollectionManagerFactory
     * @instance
     * @todo add option to delete items in collection?
     *
     * @param {string} key collection key
     * @return void
     */
    removeCollection: function removeCollection(key) {
      delete this.collections[key];
    },

    /**
     *
     * Checks if collection exists
     *
     * @memberOf module:MapApi/CollectionManagerFactory
     * @instance
     *
     * @param {string} key collection key
     * @return {bool}
     */
    collectionExists: function collectionExists(key) {
      return this.collections[key] ? true : false;
    }
  }
});

const CollectionManagerFactory = stampit(InitiateProps, PublicMethods);

/**
 *
 * Collection manager factory
 *
 * @module MapApi/CollectionManagerFactory
 *
 * @example
 * import CollectionManagerFactory from './map-api/collection-manager-factory'
 *
 * let collectionManager = CollectionManagerFactory.create();
 *
 * // Add new collection to manager
 * let markerCollection = collectionManager.addCollection('markers');
 *
 */
export default CollectionManagerFactory;
