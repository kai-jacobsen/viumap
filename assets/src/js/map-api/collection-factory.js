import stampit from 'stampit';




const PrivilegedMethods = stampit({
  init() {
    let collection = {};

    /**
     *
     * Adds item to collection
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @param {string} id item identifier in collection
     * @param {object} item instance
     *
     * @return void
     */
    this.addItem = function addItem(id, item) {

      collection[id] = item;
    };

    /**
     *
     * Remove item from collection based on id
     * and optionally remove it from map
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @param {string} id
     * @param {bool} [removeFromMap=true] if item should be removed from map
     * @return void
     */
    this.removeItem = function removeItem(id, removeFromMap = true) {

      if (removeFromMap) {
        collection[id].setMap(null);
      }

      delete collection[id];
    };

    /**
     *
     * Get collection of item instances
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @return {object} collection of items
     */
    this.getCollection = function getCollection() {
      return collection;
    };
  }
});



const PublicMethods = stampit({
  methods: {

    /**
     *
     * Hide all items in collection by calling hide method
     * of all item instances.
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @return void
     */
    hide: function hide() {
      let col = this.getCollection();

      for (let key in col) {
        if (col.hasOwnProperty(key)) {
          let item = col[key];

          item.hide();

          if (typeof item.closeWindow === 'function') {
            item.closeWindow();
          }
        }
      }
    },

    /**
     *
     * Show all items in collection by calling show method
     * of all item instances.
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @return void
     */
    show: function show() {
      let col = this.getCollection();

      for (let key in col) {
        if (col.hasOwnProperty(key)) {
          col[key].show();
        }
      }
    },

    /**
     *
     * Get item from collection based on id
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @param {string} id id of item
     * @return {object} item instance created with fx. {@link module:MapApi/Google/MarkerApiFactory|MapApi/Google/MarkerApiFactory}
     */
    getItem: function getItem(id) {
      return this.getCollection()[id] || null;
    },

    /**
     *
     * Get all items from collection
     *
     * @memberOf module:MapApi/CollectionFactory
     * @instance
     *
     * @return {object[]} array of item instances created with fx. {@link module:MapApi/Google/MarkerApiFactory|MapApi/Google/MarkerApiFactory}
     */
    getAllItems: function getAllItems() {
      let col = this.getCollection(),
        itemsArray = [];

      for (let key in col) {
        if (col.hasOwnProperty(key)) {
          itemsArray.push(col[key]);
        }
      }

      return itemsArray;
    },
  }
});


const CollectionFactory = stampit(PrivilegedMethods, PublicMethods);


/**
 *
 * Google maps collection manager factory
 *
 * @module MapApi/CollectionFactory
 *
 * @example
 * import CollectionFactory from './map-api/collection-factory'
 *
 * // or CollectionFactory.create();
 * let collection = CollectionFactory();
 *
 * // Add marker instance to collection
 * collection.addItem('idString', markerApiInstance);
 *
 */
export default CollectionFactory;
