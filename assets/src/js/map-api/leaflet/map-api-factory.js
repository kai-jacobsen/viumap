import stampit from 'stampit';
import Utils from './utils-mixin';


const PrivilegedMethods = stampit({
  init() {
    let mapObj;

    /**
     *
     * Get map or return null if no map was created
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @function getMap
     * @instance
     *
     * @return {?object} L.map leaflet maps instance
     */
    this.getMap = function getMap() {
      return mapObj || null;
    };

    /**
     *
     * Sets leaflet map instance value to be returned with getMap
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @function setMap
     * @instance
     *
     * @param {object} L.map leaflet maps instance
     * @return void
     */
    this.setMap = function setMap(map) {
      mapObj = map;
    };
  }
});

const InitiateProps = stampit({

  /**
   *
   * Initializes leaflet maps handler with needed data.
   * Is called automatically with options passed to
   * main factory method / create method
   *
   * @ignore
   *
   * @param {object} opt object for initializing, see export below for all option parameters.
   * @param {object} opt.data
   * @param {object} [opt.options={}] additional {@link http://leafletjs.com/reference-1.2.0.html#map|map options}
   *
   * @return void
   */
  init: function init({data, options={}}, {instance, stamp, args}) {
    this.data = data;
    this.options = options;
  },

  properties: {
    data: {},
    options: {}
  }
});


const PublicMethods = stampit({

  methods: {
    /**
     *
     * Pans map to coordinates
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @param {number} lat
     * @param {number} lng
     *
     * @return {object} The instance on which this method was called.
     */
    panTo: function panTo(lat, lng) {
      let latLngObj = this.createLatLng(lat, lng);

      this.getMap().panTo(latLngObj, {animate: false});

      return this;
    },

    /**
     *
     * Adds map to DOM
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @return {object} The instance on which this method was called.
     */
    addMap: function addMap() {

      let mapObj,
        mapId = this.data.id,
        mapOptions,
        tileLayerOptions,
        mapDataOptions,
        mapBounds;

      // Set map options
      mapOptions = this.getMapOptions();

      // If map uses a raster tile layer, we need to create
      // map bounds based on said layer size
      // if (this.data.tilesPath) {

      //   // Create map object
      //   mapObj = L.map(mapId, mapOptions).setView([0, 0], mapOptions.zoom);

      //   mapBounds = L.latLngBounds(
      //     mapObj.unproject([ 0, this.data.height ], mapOptions.maxZoom),
      //     mapObj.unproject([ this.data.width, 0 ], mapOptions.maxZoom));

      //   tileLayerOptions = this.getTileLayerOptions();

      //   L.tileLayer(this.data.tilesPath + '/{z}/{x}/{y}.png', tileLayerOptions).addTo(mapObj);

      //   // Fit bounds with value calculated above, and set
      //   // zoom value after, as map init zoom option is overridden when fitting to bounds
      //   mapObj.fitBounds(mapBounds, {animate:false}).setZoom(mapOptions.zoom, {animate: false});
      // } else {


      // Create map object
      mapObj = L.map(mapId, mapOptions).setView(mapOptions.center, mapOptions.zoom);

      L.tileLayer(this.data.tileServerUrl, {
        minZoom: mapOptions.minZoom,
        maxZoom: mapOptions.maxZoom,
        attribution: this.data.attribute || ''
      }).addTo(mapObj);
      //}

      this.setMap(mapObj);

      return this;
    },

    /**
     *
     * Creates object of merged tilelayer options and returns it
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @return {object} tilelayer options object
     */
    // getTileLayerOptions: function getTileLayerOptions(){
    //   let mapDataOptions,
    //     //mergedTilelayerOptions,
    //     minZoom = this.data.minZoom || 2,
    //     maxZoom = this.data.maxZoom || 6,
    //     attribute = this.data.attribute || '';

    //   mapDataOptions = {
    //     minZoom: minZoom,
    //     maxZoom: maxZoom,
    //     attribution: attribute,
    //     noWrap: true
    //   };

    //   return mapDataOptions;
    // },

    /**
     *
     * Creates object of merged map options and returns it
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @return {object} map options object
     */
    getMapOptions: function getMapOptions(){
      let mapDataOptions,
        mergedMapOptions,
        zoom = this.data.initialZoom || 0,
        minZoom = this.data.minZoom || undefined,
        maxZoom = this.data.maxZoom || undefined,
        zoomControl = Number(this.data.hideZoom) === 1 ? false : true,
        attributionControl = Number(this.data.hideAttribution) === 1 ? false : true,
        doubleClickZoom = Number(this.data.disableDoubleClick) === 1 ? false : true,
        scrollWheelZoom = Number(this.data.disableScrollZoom) === 1 ? false : true,
        dragging = Number(this.data.disableDragging) === 1 ? false : true,
        touchZoom = Number(this.data.disableTouchZoom) === 1 ? false : true,
        center = this.createLatLng(this.data.centerLat || 0, this.data.centerLng || 0);

      mapDataOptions = {
        center: center,
        zoom: zoom,
        minZoom: minZoom,
        maxZoom: maxZoom,
        zoomControl:zoomControl,
        attributionControl:attributionControl,
        doubleClickZoom: doubleClickZoom,
        dragging: dragging,
        scrollWheelZoom: scrollWheelZoom,
        touchZoom: touchZoom,
        // always set to false to prevent
        // state where map disappears when zoomed
        // too far out
        bounceAtZoomLimits: false,
        //crs: L.CRS.Simple
      };

      mergedMapOptions = Object.assign(mapDataOptions, this.options);

      return mergedMapOptions;
    },

    /**
     *
     * Gets center of map
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @return {object} L.latLng object instance
     */
    getCenter: function getCenter() {
      return this.getMap().getCenter();
    },

    /**
     *
     * Sets center of map
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @param {number} lat
     * @param {number} lng
     *
     * @return {object} The instance on which this method was called.
     */
    setCenter: function setCenter(lat, lng) {
      this.panTo(lat, lng);

      return this;
    },

    /**
     *
     * Resizes map for proper rendering,
     * fx. if map container changes size
     *
     * @memberOf module:MapApi/Leaflet/MapApiFactory
     * @instance
     *
     * @return {object} The instance on which this method was called.
     */
    resize: function resize() {
      this.getMap().invalidateSize();

      return this;
    },

    /**
     *
     * Map on ready function, triggered on 'idle', which accepts
     * callback. Event listener is only called once
     *
     * @param {function} cb callback
     * @return {object} The instance on which this method was called.
     */
    onReady: function onReady(cb){
      let instance = this;

      this.getMap().whenReady(function onReadyCallback(){
        cb.call(instance);
      });

      return this;
    },

    /**
     *
     * Fit map bounds to array of items
     *
     * @param {object[]} items array of instances created with fx. {@link module:MapApi/Leaflet/MarkerApiFactory|MapApi/Leaflet/MarkerApiFactory}
     *
     * @return {object} The instance on which this method was called.
     */
    fitToItems: function fitToItems(items=[]){
      if (items.length) {

        let map = this.getMap(),
          leafletItemsArray;

        leafletItemsArray = items.map(function(el) {
          return el.getItem();
        });

        // Fit to item bounds and zoom out an extra step,
        // to ensure all markers are visible
        map.fitBounds(
          L.featureGroup(leafletItemsArray).getBounds(), {animate:false}
        ).setZoom(map.getZoom() - 1, {animate:false});

      }

      return this;
    }
  }

});


const MapApiFactory = stampit(PrivilegedMethods, InitiateProps, PublicMethods, Utils);


/**
 *
 * Leaflet maps api factory created using stampit
 *
 * @module MapApi/Leaflet/MapApiFactory
 * @mixes MapApi/Leaflet/UtilsMixin
 *
 * Initializes leaflet maps handler with needed data.
 *
 * @param {object}              opt                                  object for initializing. Note that opt.data keys are based on data-attribute names
 *                                                                   and are not the same as the actual leaflet map option keys. The actual keys can be used
 *                                                                   via opt.options
 * @param {object}              opt.data
 * @param {string}              opt.data.id                          map dom container id
 * @param {string}              [opt.data.tilesPath]                 path for raster tiles. Fx. '/fileadmin/content/maps/testmap/'
 * @param {string|number}       [opt.data.width]                     width of generated raster tiles using fx. maptiler when fully zoomed. Required when using tilesPath
 * @param {string|number}       [opt.data.height]                    height of generated raster tiles using fx. maptiler when fully zoomed. Required when using tilesPath
 * @param {string|number}       [opt.data.centerLat]                 default center latitude of map
 * @param {string|number}       [opt.data.centerLng]                 default center longitude of map
 * @param {string|number}       [opt.data.minZoom]                   min zoom value of map. Required when using tilesPath
 * @param {string|number}       [opt.data.maxZoom]                   max zoom value of map. Required when using tilesPath
 * @param {string|number}       [opt.data.initialZoom]               initial zoom value of map
 * @param {bool|string|number}  [opt.data.hideZoom=false]            hide zoom controls
 * @param {bool|string|number}  [opt.data.disableDoubleClick=false]  disable zoom via doubleclick
 * @param {bool|string|number}  [opt.data.disableDragging=false]     disable dragging map via mouse/touch
 * @param {bool|string|number}  [opt.data.disableScrollZoom=false]   disable zoom via mouse scroll
 * @param {bool|string|number}  [opt.data.disableTouchZoom=false]    disable zoom via touch
 * @param {bool|string|number}  [opt.data.hideAttribution=false]     hide leaflet attribution link
 * @param {object}              [opt.options={}]                     additional {@link http://leafletjs.com/reference-1.2.0.html#map|map options},
 *                                                                   will override any existing options which were set via opt.data
 *
 * @example
 * import MapApiFactory from './map-api/leaflet/map-api-factory'
 *
 * // or MapApiFactory.create({data, options});
 * let mapHandler = MapApiFactory({
 *   data: {
 *     id: 'some-id',
 *     disableDragging: '1'
 *   },
 *   options: {
 *     dragging: true // overrides opt.data.disableDragging
 *   }
 * });
 *
 * mapHandler.addMap(); // adds map to DOM
 *
 */
export default MapApiFactory;
