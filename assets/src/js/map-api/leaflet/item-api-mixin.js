import stampit from 'stampit';
import Utils from './utils-mixin';


const PrivilegedMethods = stampit({
  init() {
    let itemObj,
      mapObj,
      infoWindowObj;

    /**
     *
     * Get item or return null if no item was created
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @function getItem
     * @instance
     *
     * @return {object} leaflet item instance: marker (L.marker), polygon etc.
     */
    this.getItem = function() {
      return itemObj || null;
    };

    /**
     *
     * Set main item object
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @function setItem
     * @instance
     *
     * @param {object} item leaflet item instance: marker, polygon etc.
     * @return {object} The instance on which this method was called
     */
    this.setItem = function(item) {
      itemObj = item;

      return this;
    };
  }
});





const PublicMethods = stampit({
  methods: {


    /**
     *
     * Add marker info window and click event listener
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @param {string} content html content
     * @param {object} [options={}] additional {@link http://leafletjs.com/reference-1.2.0.html#popup|popup options}
     *
     * @return {object} The instance on which this method was called
     */
    addInfoWindow: function addInfoWindow(content, options = {}) {
      let item = this.getItem(),
        mergedOptions;

      mergedOptions = Object.assign({
        content: content
      }, options);

      item.bindPopup(content, mergedOptions);

      return this;
    },

    /**
     *
     * Get infoWindow of item
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {?object} L.popup object instance
     */
    getInfoWindow: function getInfoWindow() {
      let item = this.getItem(),
        infoWindowObj = null;

      if (item) {
        infoWindowObj = item.getPopup();
      }

      return infoWindowObj;
    },

    /**
     *
     * Opens item window if one exists
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {object} The instance on which this method was called
     */
    openWindow: function openWindow() {

      this.getItem().openPopup();

      return this;
    },

    /**
     *
     * Closes item window if one exists
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {object} The instance on which this method was called
     */
    closeWindow: function openWindow() {
      let w = this.getInfoWindow();

      if (w) {
        w.closePopup();
      }

      return this;
    },

    /**
     *
     * Get map to which item is currently attached
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {?object} L.map object instance
     */
    getMap: function getMap() {
      let item = this.getItem(),
        map;

      if (item) {
        map = item.options.map;
      }

      return map || null;
    },

    /**
     *
     * Set map of item
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @param {object} L.map object instance
     *
     * @return {object} The instance on which this method was called
     */
    setMap: function setMap(map) {
      let item = this.getItem();

      if (item) {
        item.addTo(map);
      }

      return this;
    }
  }
});

const ItemApiMixin = stampit(PrivilegedMethods, PublicMethods, Utils);

/**
 *
 * leaflet item api mixin created using stampit.
 *
 * Serves as base mixin for all leaflet map item type
 * factories like markers, polygons etc.
 *
 * @mixin MapApi/Leaflet/ItemApiMixin
 *
 * @example
 * import ItemApiMixin from './map-api/leaflet/item-api-mixin';
 *
 * // create new item type factory
 * let NewItemFactory = stampit(ItemApiMixin, SomeNewProps, SomeNewMethods)
 *
 * let newItemHandler = NewItemFactory.create()
 */
export default ItemApiMixin;
