import stampit from 'stampit';

const UtilsMixin = stampit({
  methods: {
    /**
     *
     * Creates latitude/longitude object
     *
     * @param {number} lat
     * @param {number} lng
     * @return {object} leaflet L.latLng object
     */
    createLatLng: function createLatLng(lat, lng) {
      return L.latLng(lat, lng);
    }
  }
});

/**
 *
 * Leaflet maps utils for use in
 * leaflet map apis
 *
 * @mixin MapApi/Leaflet/UtilsMixin
 */
export default UtilsMixin;
