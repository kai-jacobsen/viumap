import stampit from 'stampit';
import ItemApiMixin from './item-api-mixin';



const InitiateProps = stampit({

  /**
   *
   * Creates leaflet maps marker
   *
   * @memberOf module:MapApi/Leaflet/MarkerApiFactory
   * @instance
   *
   * @ignore
   *
   * @param {object} opt object for initializing
   * @param {object} opt.data
   * @param {number} opt.data.lat      latitude
   * @param {number} opt.data.lng      longitude
   * @param {object} opt.data.map      map to attach marker to
   * @param {object} [opt.options={}]  additional {@link http://leafletjs.com/reference-1.2.0.html#marker|marker options}
   *
   * @return {object} The instance on which this method was called
   */
  init: function init({data, options={}}, {instance, stamp, args}) {
    let markerLatLng,
      marker,
      mergedOptions,
      defaultOptions;

    this.data = data;
    this.options = options;

    markerLatLng = this.createLatLng(data.lat, data.lng);

    defaultOptions = {
      map: data.map
    };

    // Create icon
    if (data.icon) {
      defaultOptions.icon = this.createIcon(data.icon);
    }

    mergedOptions = Object.assign(defaultOptions, options);

    marker = L.marker(markerLatLng, mergedOptions);

    this.setItem(marker);

    this.setMap(data.map);

    return this;
  },

  properties: {
    data: {},
    options: {}
  }
});


const PublicMethods = stampit({
  methods: {

    /**
     *
     * Get marker lat/lng position object
     *
     * @memberOf module:MapApi/Leaflet/MarkerApiFactory
     * @instance
     *
     * @return {object} L.latLng object instance
     */
    getPosition: function getPosition() {
      return this.getItem().getLatLng();
    },

    /**
     *
     * Set position of marker using options object
     *
     * @memberOf module:MapApi/Leaflet/MarkerApiFactory
     * @instance
     *
     * @param {object} options      lat/lng
     * @param {number} options.lat  latitude
     * @param {number} options.lng  longitude
     *
     * @return {object} The instance on which this method was called
     */
    setPosition: function setPosition(options) {
      let markerLatLng = this.createLatLng(options.lat, options.lng);

      this.getItem().setLatLng(markerLatLng);

      return this;
    },

    /**
     *
     * Creates icon for use in marker
     *
     * @memberOf module:MapApi/Leaflet/MarkerApiFactory
     * @instance
     *
     * @param {object}    options                    options object for configuring icon
     * @param {string}    options.iconUrl            url of icon
     * @param {string}   [options.iconRetinaUrl]    url of retina icon
     * @param {number[]} [options.iconSize]         natural width/height of non-retina icon, fx [40,80]
     * @param {number[]} [options.iconAnchor]       xy coordinates of marker anchor relative to icon, fx [20,40]
     * @param {number[]} [options.popupAnchor]      xy coordinates of popup anchor relative to marker anchor, fx [20,-40]
     * @param {string}   [options.shadowUrl]        url of shadow icon
     * @param {string}   [options.shadowRetinaUrl]  url of retina shadow icon
     * @param {number[]} [options.shadowSize]       natural width/height of non-retina shadow, fx [70,40]
     * @param {number[]} [options.shadowAnchor]     xy coordinates of shadow icon to use as anchor, fx [0,40]
     * @param {string}   [options.className]        class name for shadow and icon img tags
     *
     * @see http://leafletjs.com/reference-1.2.0.html#icon-l-icon
     *
     * @return {object} L.icon object instance
     */
    createIcon: function createIcon(options) {

      let icon = L.icon(options);

      return icon;
    },

    /**
     *
     * Set icon of marker
     *
     * @memberOf module:MapApi/Leaflet/MarkerApiFactory
     * @instance
     *
     * @param {object} options see {@link module:MapApi/Leaflet/MarkerApiFactory|createIcon method} for params
     *
     * @return {object} The instance on which this method was called
     */
    setIcon: function setIcon(options) {
      let icon = this.createIcon(options);

      this.getItem().setIcon(icon);

      return this;
    },

    /**
     *
     * Show item
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {object} The instance on which this method was called
     */
    show: function show() {
      this.getItem().setOpacity(1);

      return this;
    },

    /**
     *
     * Hide item
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {object} The instance on which this method was called
     */
    hide: function hide() {
      this.getItem().setOpacity(0);

      return this;
    },

    /**
     *
     * Checks if marker is hidden
     *
     * @memberOf MapApi/Leaflet/ItemApiMixin
     * @instance
     *
     * @return {bool} if marker is hidden or not
     */
    isHidden: function isHidden() {
      let hidden = this.getItem().options.opacity === 0;

      return hidden;
    }
  }

});

const MarkerApiFactory = stampit(ItemApiMixin, InitiateProps, PublicMethods);

/**
 *
 * Leaflet marker api factory created using stampit
 *
 * @module MapApi/Leaflet/MarkerApiFactory
 * @mixes MapApi/Leaflet/ItemApiMixin
 *
 * @param {object} opt object for initializing
 * @param {object} opt.data
 * @param {number} opt.data.lat      latitude
 * @param {number} opt.data.lng      longitude
 * @param {object} opt.data.map      map to attach marker to
 * @param {object} [opt.data.icon]   see {@link module:MapApi/Leaflet/MarkerApiFactory#createIcon|createIcon method} for params
 * @param {object} [opt.options={}]  additional {@link http://leafletjs.com/reference-1.2.0.html#marker|marker options}
 *
 * @example
 * import MarkerApiFactory from './map-api/leaflet/marker-api-factory'
 *
 * // or MarkerApiFactory.create({data, options});
 * let markerHandler = MarkerApiFactory({
 *   data:{
 *     lat: markerData.lat,
 *     lng: markerData.lng,
 *     map: mapHandler.getMap(),
 *   }, options: {}
 * }).addInfoWindow(htmlString);
 */
export default MarkerApiFactory;

