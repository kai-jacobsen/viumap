import Vue from './vue-configured';

import markerInfoWindow from './components/marker-info-window.vue';
import markerLegend from './components/marker-legend.vue';
import eventHub from './components/event-hub';


const rootId = 'vm-app';

let vm;

(function(){
  if (!document.getElementById(rootId)) {
    return;
  }


  /**
   *
   * Main App
   *
   */
  vm = new Vue({
    el: '#' + rootId,
    data: {
      apiData: {},
      activeMarkerIndex: -1
    },
    components: {
      markerInfoWindow,
      markerLegend
    },

    mounted () {
      //this.setData();
    },

    created () {
      eventHub.$on('hideMarkerInfoWindow', () => {
        this.hideActiveMarker();
      });
    },

    methods: {

      /**
       *
       * Sets active marker index. Use minus
       *
       * @param {number} index
       * @return void
       */
      setActiveMarkerIndex(index) {
        this.$set(this, 'activeMarkerIndex', index);
      },

      /**
       *
       * Hides active marker
       *
       * @return void
       */
      hideActiveMarker() {
        this.setActiveMarkerIndex(-1)
      }

    },

    computed: {
      activeMarker() {
        let data = this.apiData,
          marker = {};

        if (data.markers && data.markers[this.activeMarkerIndex]) {
          marker = data.markers[this.activeMarkerIndex];
        }

        return marker;
      }
    }
  });

})();

export default vm;




