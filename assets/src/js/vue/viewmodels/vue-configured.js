import Vue from 'vue';

Vue.config.devtools = true;

Vue.mixin({
  delimiters: ['${', '}']
});


if (process.env.NODE_ENV === 'production') {
  Vue.config.silent = true;
  Vue.config.devtools = false;
}

export default Vue;
