import Vue from './vue/viewmodels/vue-configured';
import vueApp from './vue/viewmodels/app';

import mapUtils from './map/utils';
import MapFactory from './map-api/leaflet/map-api-factory';
import MarkerFactory from './map-api/leaflet/marker-api-factory';
import MapInteractionFactory from './map/map-interaction-factory';


/**
 *
 * Renders maps, polygons and adds click handlers to them
 *
 * @param {object} apiData api data array
 *
 * @return void
 */
function renderMaps(apiData) {
  let outerMapDomEls = document.querySelectorAll('.js-multimaps-map-outer'),
    geo = JSON.parse(apiData.geojson);

  outerMapDomEls.forEach(function(outerMapEl){

    let mapDomEl = outerMapEl.querySelector('.js-multimaps-map'),
      markers = outerMapEl.querySelectorAll('.js-map-marker'),
      data = mapDomEl.dataset,
      mapHandler,
      mapInteractionHandler,
      mapObj,
      intersectedPoly,
      options,
      polygons = mapUtils.getPolygonsFromFeatureCollection(geo),
      boundsObj = mapUtils.getPolygonBounds(polygons),
      finalPoly = mapUtils.getIntersectedPolygon(geo, apiData);

    // Create map handler instance using factory method
    // passing needed options
    mapHandler = MapFactory({data});

    // Add map to DOM
    mapHandler.addMap();

    // get created map object
    mapObj = mapHandler.getMap();

    // Don't allow panning outside world bounds
    mapObj.setMaxBounds([[-90, 180], [90, -180]]);

    // Add intersected polygon
    finalPoly.addTo(mapObj);

    // Fit map to bounds of polygon used to intersect
    mapObj.fitBounds(boundsObj);

    // Create interaction handler
    mapInteractionHandler = MapInteractionFactory(mapHandler);

    // Add click handlers to polygons and add click callback
    mapInteractionHandler.addPolygons(polygons, function(event, lat, lng){
      handleShowFormClick(event, lat, lng);
    });

    // If markes are available via api, render them
    if (apiData.markers) {
      // Add markers and pass click callback
      mapInteractionHandler.addMarkers(apiData.markers, function(event, markerHandler, markerIndex){
        handleMarkerClick(markerHandler, markerIndex);
      });
    }
  });
}

/**
 *
 * Handles clicks on markers which are already present
 * in the api data
 *
 * @param {object} markerHandler MapApi/Leaflet/MarkerApiFactory instance
 * @param {number} markerIndex index of marker in api data array
 *
 * @return void
 */
function handleMarkerClick(markerHandler, markerIndex) {
  vueApp.setActiveMarkerIndex(markerIndex);
}

/**
 *
 * Handles clicks on user-created marker
 * to show popup form
 *
 * @param {object} event click event
 * @param {number} lat marker lat
 * @param {number} lng marker lng
 *
 * @return void
 */
function handleShowFormClick(event, lat, lng){
  let $formPopup = $('.js-form-popup');

  $formPopup.addClass('active');

  $formPopup.find('.js-lat').val(lat);
  $formPopup.find('.js-lng').val(lng);
}



$(document).ready(function() {

  $.ajax({
    url: 'http://wideviu.de/wp-json/viu/v1/maps/136/',
  }).done(function(data) {
    Vue.set(vueApp, 'apiData', data);
    renderMaps(data);
  });

  $('.js-close').on('click', function(){
    $(this).parents('.js-popup').removeClass('active');
  });

});
