

// Copy this file to config.js and adjust domain settings

module.exports = {
  serverHostProtocol: 'http://',
  serverHost: 'somedomain.test',
  serverPort: 8080,
  serverHostFull: 'http://somedomain.test:8080'
};
