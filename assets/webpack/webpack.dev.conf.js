const merge = require('webpack-merge');
const path = require('path');
const fs = require('fs');
const baseWebpackConfig = require('./webpack.base.conf');
const config = require('./config');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const assetFunctions = require('node-sass-asset-functions');

const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');

const webpackDevConfig = merge(baseWebpackConfig, {

  output: {
    publicPath: config.serverHostFull + '/dist/'
  },

  // Enable detection when saving directly in
  // chrome dev tools
  devtool: 'inline-source-map',

  // Bundle server files into separate js file
  // otherwise the would be inlined into main bundle
  entry: {
    server: [
      'webpack-dev-server/client?' + config.serverHostFull,
      'webpack/hot/dev-server'
    ]
  },

  module: {
    rules: [{

      // NOTE: dev css rule uses dev server
      // which serves files from memory, .i.e. does not
      // write files to disk
      test: /\.scss$/,
      use: ['css-hot-loader'].concat(ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              minimize: false,
              discardComments: {
                removeAll: true
              }
            }
          },
          {
            loader: 'resolve-url-loader'
          },
          {
            loader: 'sass-loader',
            options: {
              outputStyle: 'compact',
              sourceMap: true,
              includePaths: [
                './node_modules/foundation-sites/scss'
              ],
              precision: 15,
              functions: assetFunctions({
                http_images_path: '/dist/img',
                images_path:  path.resolve(__dirname, '../src/img'),
                asset_cache_buster: function(http_path, real_path, done){

                  fs.stat(real_path, (err, stats) => {
                    let ts;

                    if (err) {

                    } else {
                      ts = 'v=' + (+new Date(stats.mtime));
                    }

                    done(ts);

                  });
                }
              })
            }
          }
        ],
      }))
    }]
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
      DEBUG: true
    }),

    // Enable use of module names (defaults to paths)
    // instead of module ids
    new webpack.NamedModulesPlugin(),

    // Enable HMR
    new webpack.HotModuleReplacementPlugin(),

    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module, count) {
        // any required modules inside node_modules are extracted to vendor
        return (
          module.resource &&
          /\.js$/.test(module.resource) &&
          module.resource.indexOf(
            path.join(__dirname, '../node_modules')
          ) === 0
        );
      }
    }),

    // Chunk with webpack runtime and manifest
    new webpack.optimize.CommonsChunkPlugin({
      name: ['manifest'],
      minChunks: Infinity,
    }),
  ],

  devServer: {
    hot: true,
    inline: false,
    host: config.serverHost,
    port: config.serverPort,
    publicPath: config.serverHostFull + '/dist/',
    allowedHosts: ['*'],
    headers: { 'Access-Control-Allow-Origin': '*' }
  }
});


module.exports = webpackDevConfig;
