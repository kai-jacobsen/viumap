const merge = require('webpack-merge');
const path = require('path');
const fs = require('fs');
const webpack = require('webpack');
const baseWebpackConfig = require('./webpack.base.conf');
const assetFunctions = require('node-sass-asset-functions');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
const ChunkManifestPlugin = require('chunk-manifest-webpack-plugin');
const WebpackChunkHash = require('webpack-chunk-hash');
const WebpackNotifierPlugin = require('webpack-notifier');

const webpackProdConfig = merge(baseWebpackConfig, {

  watchOptions: {
    poll: 1000
  },

  output: {
    publicPath: '/dist/',
    // Hashing for generated html files
    // filename: 'js/[name].[chunkhash].js',
    // chunkFilename: 'js/[name].[chunkhash].js',

    // For use in external html templates,
    // we dont hash filenames, the external
    // handling takes care of that (typo3/wordpress etc.)
    // TODO: how does this affect manifest.json async loading
    // of cached assets?
    filename: 'js/[name]/[name].bundle.js',
    chunkFilename: 'js/[name]/[name].bundle.js',
  },

  devtool: 'source-map',

  module: {
    rules: [{
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        use: [{
          loader: 'css-loader'
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
            plugins: function () {
              return [autoprefixer];
            }
          }
        },
        {
          loader: 'sass-loader',

          // Node-sass options
          options: {
            sourceMap: true,
            outputStyle: 'compressed',
            includePaths: [
              './node_modules/foundation-sites/scss'
            ],
            precision: 15,
            // Node-sass / libsass has no built in image helpers
            // so we add assetFunctions and custom cachebuster
            functions: assetFunctions({
              http_images_path: '/dist/img',
              images_path:  path.resolve(__dirname, '../src/img'),
              asset_cache_buster: function(http_path, real_path, done){
                fs.stat(real_path, (err, stats) => {
                  let ts;

                  if (err) {

                  } else {
                    ts = 'v=' + (+new Date(stats.mtime));
                  }

                  done(ts);

                });
              }
            })
          }
        },
        // {
        //   loader: 'resolve-url-loader'
        // }
        ]
      })
    }]
  },

  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production',
      DEBUG: false
    }),

    // Enable scope hoisting
    // @see: https://medium.com/webpack/brief-introduction-to-scope-hoisting-in-webpack-8435084c171f
    new webpack.optimize.ModuleConcatenationPlugin(),

    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true
    }),

    // split vendor js into its own file
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendor',
      minChunks: function (module, count) {
        // any required modules inside node_modules are extracted to vendor
        return (
          module.resource &&
          /\.js$/.test(module.resource) &&
          module.resource.indexOf(
            path.join(__dirname, '../node_modules')
          ) === 0
        );
      }
    }),

    // Example of common chunks, useful for larger projects,
    // or js components (vue/react)
    // @see: https://github.com/webpack/webpack/tree/master/examples/multiple-commons-chunks
    // new webpack.optimize.CommonsChunkPlugin({
    //   name: 'commons',
    //   chunks: ['app1', 'app2', 'app3']
    // }),

    // extract webpack runtime and module manifest to its own file in order to
    // prevent vendor hash from being updated whenever app bundle is updated
    new webpack.optimize.CommonsChunkPlugin({
      name: ['manifest'],
      minChunks: Infinity,
    }),

    // Needed for consistent chunkhash calculations
    // otherwise manifest/runtime hash changes when
    // files are edited
    new webpack.HashedModuleIdsPlugin(),
    new WebpackChunkHash(),

    new ChunkManifestPlugin({
      filename: 'manifest.json',
      manifestVariable: 'webpackManifest',
      inlineManifest: true
    }),

    new WebpackNotifierPlugin({
      title: 'Build-process',
      alwaysNotify: true
    }),

  ]
});

module.exports = webpackProdConfig;
