const path = require('path');

const config = require('./config');


const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');



const webpackBaseConfig = {

  entry: {
    app: [
      './src/js/app.js',
      './src/sass/main.scss'
    ]
  },

  externals: {
    jquery: 'jQuery'
  },

  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'js/[name]/[name].bundle.js',
  },

  target: 'web',

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },

  module: {

    rules: [

      {
        // regular css files
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: ['css-loader?importLoaders=1', 'resolve-url-loader'],
        }),
      },
      {
        test: /\.(png|woff|woff2|eot|ttf|svg)$/,
        loader: 'url-loader?limit=100000'
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        // Transpile to es2015 using babel
        test: /\.js$/,
        loader: 'babel-loader',
        //exclude: /(node_modules|bower_components)/,
        include: [
          path.resolve(__dirname, '../src'),
          path.resolve(__dirname, '../node_modules/foundation-sites')
        ]
      }
    ]
  },


  plugins: [

    // Copy the images folder...
    new CopyWebpackPlugin([{
      from: './src/img/',
      to: 'img',
      ignore: '.DS_Store',
    }]),

    new ExtractTextPlugin({
      filename: 'css/[name]/[name].bundle.css',
      allChunks: true
    }),

    // Create test-html file
    // new HtmlWebpackPlugin({
    //   title: 'Output Management',
    //   filename: '../index.html'
    // }),

    // ...and optimize all the images
    new ImageminPlugin({
      test: /\.(jpe?g|png|gif|svg)$/i,
      optipng: {
        optimizationLevel: 9
      }
    })
  ]
};


module.exports = webpackBaseConfig;
